/* 
 * This is less a JavaScript file than a config file that
 * uses JavaScript to handle some path data that should
 * not really be hard-coded into the rest of the 
 * JavaScript file set. Update this file as needed.
 */

let configpaths = { 
	version: "1", 
	scanpath: ["/Volumes/ADATA_UFD/videos/"], 
	indexpath: "http://192.168.1.3/cgi-bin/indexmedia.cgi", 
	cgipath: "http://192.168.1.3/cgi-bin/getmedia.cgi" 
};
