/* DownloadHelper.js
 * Created by: Erik Hentell
 *
 * This is just some basic code to facilitate the population of 
 * folder hierarchies to scan and the assembly of download links
 * from which to pull files
 */

/* Pulls data from configpaths.js and uses it to populate a
 * dropdown list of folder choices
 *
 * arguments: none
 * returns: none
 * side effects: appends the <select id="scandirectory"> element
 */
function buildScanOptions() {
	var basestring = "<option value=\"REPLACE\">REPLACE</option>";
	var optionstring = ""
	
	var pathlen = configpaths.scanpath.length;

	while (pathlen >= 1) {
		optionstring = optionstring + basestring.replace(/REPLACE/g, configpaths.scanpath[pathlen - 1]);

		$('select#scandirectory').append(optionstring);

		pathlen = pathlen - 1;
	}

}

function indexCGI() {
	var thescan = $("#scandirectory").val();
	var thequery = "versionnum=" + configpaths.version + "&scanpath=" + thescan + "&cgipath=" + configpaths.cgipath;
	
	$("div#linkbox").load(configpaths.indexpath, thequery, function(response, status, xhr) {
		if (status == "error") {
			var msg = "<p>There was an error loading the data</p>Status: ";
			$("div#linkbox").append(msg + xhr.status + " " + xhr.statusText);
		} else {
			$("div#linkbox").append(response);
		}
	});
}
