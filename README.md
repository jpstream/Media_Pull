# MediaPull

Media Pull is comprised of a set of simple python scripts meant to make a locally stored media files available to download somewhere else on the network.  

Background
==========
Over time I have acquired a large amount of video, audio and image files that are stored on a local computer. I enjoy going through these files on occasion, but had become tired of going to the same computer to view everything, especially since I have more than one and perform different types of work on each.

Even though there are many options available, I decided to take the opportunity to make a small set of utility scripts to index and download the files whenever and wherever I want, such as my phone, my tablet, or another computer. These scripts are comprised of a Python CGI script, a Python script for indexing a folder hierarchy, and a related set of HTML, CSS and JavaScript files.

**getmedia.cgi**
----------------
getmedia.cgi is a simple Python script that receives a file path and determines the file type before returning an HTTP binary stream. The script does not engage in HTTP streaming or act as an RTSP server in its current iteration. Perhaps there will be updates at a later time.

**indexmedia.py**
-----------------
indexmedia.py is a simple Python script that walks through a folder hierarchy and locates files with a specified extension. As of this writing, the files are specific video formats, but I will update with audio and image formats later. Once the hierarchy is determined, the script pulls text from a pair of template files and generates an HTML file to be placed at some user-defined location. Some CSS and JavaScript files are also supplied to help round things out.

To use indexmedia.py, run some variation of: "python path-to-output-file path-of-folders-to-search url-to-getmedia.cgi". If the output file exists, it will be deleted before being created and filled with information. "html.template" and "linkcontainer.template" are used to construct a composite HTML file that contains the necessary link tags. "vidlink.css" is used for this file as well, so it will be necessary to have it in the same folder that the output file is stored.

WEB AND CGI USAGE
=================
* Put the getmedia.cgi, indextemplates folder, and indexmedia.cgi into whatever folder is designated as the folder for cgi scripts
* Put the mediapull.html file, the css folder, the images folder and the scripts folder somewhere in the web folder hierarchy
* Be aware that, because of cross-origin vulnerabilities, browsers may try to play media instead of downloading it. This is a browser security issue and, since this system is meant for personal use, did not seem worth trying to get around
* The configpaths.js file in the script folder is for configuration purposes. It can be modified with preferred file paths for the indexmedia.cgi script to index and return to the mediapull.html file. This is useful if, for example, there is a folder for videos, another for audio, and another for images
* Once everything is in place, open the mediapull.html file in a web browser. Use the dropdown list to select the desired folder hierarchy to index. If the folder path is accurate, a series of links should appear. Clicking on those links should result in a downloaded file. As mentioned above, due to potential CORS (Cross Origin Resource Sharing), browsers might not download a file such as an MP3 or JPEG and instead may try to display it. This is the same for video files


